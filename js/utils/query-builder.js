import md5 from 'md5'
import {PUBLIC_KEY, PRIVATE_KEY} from '../constants'

const generateCredentials = () => (
    `ts=${Date.now()}&apikey=${PUBLIC_KEY}&hash=${md5(`${Date.now()}${PRIVATE_KEY}${PUBLIC_KEY}`)}`
);

export const queryBuilder = (baseUrl, queries = []) => (
    `${baseUrl}?${queries.join('&')}&${generateCredentials()}`
);

export const limitQuery = limit => limit ? `limit=${limit}` : '';

export const offsetQuery = offset => offset ? `offset=${offset}` : '';

export const nameStartsWithQuery = nameStartsWith => nameStartsWith ? `nameStartsWith=${nameStartsWith}` : '';

export const comicsQuery = comics => comics ? `comics=${comics}` : '';

export const seriesQuery = series => series ? `series=${series}` : '';
