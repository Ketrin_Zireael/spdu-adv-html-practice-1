import {BASE_URL} from './constants'
import {
    limitQuery,
    queryBuilder,
    offsetQuery,
    nameStartsWithQuery,
    comicsQuery,
    seriesQuery
} from './utils/query-builder'

export const fetchCharactersData = queries => {
     const {limit, page, nameStartsWith, comics, series} = queries;

     const queriesArray = [
         limitQuery(limit),
         offsetQuery(limit * page),
         nameStartsWithQuery(nameStartsWith),
         comicsQuery(comics),
         seriesQuery(series)
     ];

    return fetch(queryBuilder(`${BASE_URL}/characters`, queriesArray)).then(response => (
        response.json()
    ));
};
